import 'package:core_data/auth/auth_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';
import 'package:core_network/network_data_source_service.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';

class AuthRepository implements AuthRepositoryService{

  final NetworkDataSourceService networkDataSourceService;
  final SharedPreferencesDataSourceService sharedPreferencesDataSourceService;

  AuthRepository({
    required this.networkDataSourceService,
    required this.sharedPreferencesDataSourceService
  });

  @override
  Future<BaseResponse<SignIn>?> postSignIn(SignInParam param) {
    return networkDataSourceService.postSignIn(param);
    //return Future.value(SignIn.dataDummy);
  }

  @override
  Future<void> saveSession(SignIn session) => sharedPreferencesDataSourceService.saveSession(session);

  @override
  Future<KeepSignedIn?> getKeepSignedIn() => sharedPreferencesDataSourceService.getKeepSignedIn();

  @override
  Future<void> keepSignedIn(KeepSignedIn keepSignedIn) => sharedPreferencesDataSourceService.keepSignedIn(keepSignedIn);

  @override
  Future<void> removekeepSignedIn() => sharedPreferencesDataSourceService.removekeepSignedIn();

  @override
  Future<BaseResponse<SignIn>?> postSignUp(SignUpParam param) => networkDataSourceService.postSignUp(param);

  @override
  Future<BaseResponse?> postForgot(ForgotParam param) => networkDataSourceService.postForgot(param);

  @override
  Future<Result<bool>> revokeSession() async {
    var result = await sharedPreferencesDataSourceService.removeSession();
    return ResultSuccess(data: result);
  }
}