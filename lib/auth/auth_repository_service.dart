import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';

abstract class AuthRepositoryService {
  Future<BaseResponse<SignIn>?> postSignIn(SignInParam param);
  Future<void> saveSession(SignIn session);
  Future<Result<bool>> revokeSession();
  Future<void> keepSignedIn(KeepSignedIn keepSignedIn);
  Future<void> removekeepSignedIn();
  Future<KeepSignedIn?> getKeepSignedIn();
  Future<BaseResponse<SignIn>?> postSignUp(SignUpParam param);
  Future<BaseResponse<dynamic>?> postForgot(ForgotParam param);
}
