import 'package:core_data/base/base_repository.dart';
import 'package:core_model/mappers/location_mapper.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/entities/location.dart';
import 'package:core_model/network/response/country_code.dart';
import 'package:core_model/network/response/district.dart';
import 'package:core_model/network/response/province.dart';
import 'package:core_model/network/response/regency.dart';
import 'package:core_model/network/response/sub_district.dart';
import 'package:core_network/network_data_source_service.dart';

import 'general_repository_service.dart';

class GeneralRepository extends BaseRepository implements GeneralRepositoryService{

  GeneralRepository({required this.networkDataSourceService});

  final NetworkDataSourceService networkDataSourceService;

  @override
  Future<BaseResponse<List<Province>>> getProvincies() => networkDataSourceService.getProvincies();

  @override
  Future<BaseResponse<List<Regency>>> getRegenciesByProvinceId(int provinceId) => networkDataSourceService.getRegenciesByProvinceId(provinceId);
  
  @override
  Future<BaseResponse<List<District>>> getDistrictsByRegencyId(int regencyId) => networkDataSourceService.getDistrictsByRegencyId(regencyId);
  
  @override
  Future<BaseResponse<List<SubDistrict>>> getSubDistrictsByDistrictId(int districtId) => networkDataSourceService.getSubDistrictsByDistrictId(districtId);

  @override
  Future<BaseResponse<List<CountryCode>>> getCountries(String? search) => networkDataSourceService.getCountries(search);

  @override
  Future<Result<List<LocationEntity>>> getLocationsBySubDistrict(
      String search) async {
    try {
      return getBaseResponseDataTransform(
          networkDataSourceService.getLocationBySubDistrict(search),
          onTransform: (item) => item.map((e) => e.toEntity()).toList());
    } catch (e) {
      return ResultError(error: Exception(e));
    }
  }

}