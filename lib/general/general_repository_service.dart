import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/entities/location.dart';
import 'package:core_model/network/response/country_code.dart';
import 'package:core_model/network/response/district.dart';
import 'package:core_model/network/response/province.dart';
import 'package:core_model/network/response/regency.dart';
import 'package:core_model/network/response/sub_district.dart';

abstract class GeneralRepositoryService{
  Future<BaseResponse<List<Province>>> getProvincies();
  Future<BaseResponse<List<Regency>>> getRegenciesByProvinceId(int provinceId);
  Future<BaseResponse<List<District>>> getDistrictsByRegencyId(int regencyId);
  Future<BaseResponse<List<SubDistrict>>> getSubDistrictsByDistrictId(int districtId);
  Future<BaseResponse<List<CountryCode>>> getCountries(String? search);
  Future<Result<List<LocationEntity>>> getLocationsBySubDistrict(String search);
}