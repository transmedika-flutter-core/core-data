import 'package:core_model/network/google/place/place_detail_response.dart';
import 'package:core_model/network/google/place/place_response.dart';
import 'package:core_network/network_data_source_service.dart';

import 'map_google_repository_service.dart';

class MapGoogleRepository implements MapGoogleRepositoryService{

  MapGoogleRepository({required this.networkDataSourceService});

  final NetworkDataSourceService networkDataSourceService;

  @override
  Future<PlaceDetailResponse> getPlaceDetail(String placeId, String? fields) {
    return networkDataSourceService.getPlaceDetail(placeId, fields);
  }

  @override
  Future<PlaceResponse> getPlaces(String input, String? types, String? components) {
    return networkDataSourceService.getPlaces(input,types,components);
  }


}