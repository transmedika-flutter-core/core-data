import 'package:core_model/network/google/place/place_detail_response.dart';
import 'package:core_model/network/google/place/place_response.dart';

abstract class MapGoogleRepositoryService{
  Future<PlaceResponse> getPlaces(String input, String? types, String? components);
  Future<PlaceDetailResponse> getPlaceDetail(String placeId, String? fields);
}