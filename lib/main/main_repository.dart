import 'package:core_data/main/main_repository_service.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_network/network_data_source_service.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';

class MainRepository implements MainRepositoryService{

  final NetworkDataSourceService networkDataSourceService;
  final SharedPreferencesDataSourceService sharedPreferencesDataSourceService;

  MainRepository({
    required this.networkDataSourceService,
    required this.sharedPreferencesDataSourceService
  });

  @override
  Future<SignIn?> getSession() => sharedPreferencesDataSourceService.getSession();
}