import 'package:core_model/network/response/signin.dart';

abstract class MainRepositoryService{
  Future<SignIn?> getSession();
}