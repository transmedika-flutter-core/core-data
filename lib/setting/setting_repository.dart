import 'package:core_data/setting/setting_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:core_model/helpers/mappers/localization_setting_mapper.dart';

class SettingRepository implements SettingRepositoryService {
  final SharedPreferencesDataSourceService sharedPreferencesDataSource;

  SettingRepository({required this.sharedPreferencesDataSource});

  @override
  Future<Result<LocalizationSettingEntity>> getLocalizationSetting() async {
    var localizationSetting =
        await sharedPreferencesDataSource.getLocalizationSetting();
    return ResultSuccess(data: localizationSetting?.toEntity());
  }

  @override
  Future<Result<void>> removeLocalizationSetting() async {
    var result = await sharedPreferencesDataSource.removeLocalizationSetting();
    if (result) {
      return ResultSuccess();
    }
    return ResultError(error: Exception("failed remove localization"));
  }

  @override
  Future<Result<LocalizationSettingEntity>> setLocalizationSetting(
    LocalizationSettingEntity localizationSetting,
  ) async {
    var status = await sharedPreferencesDataSource
        .setLocalizationSetting(localizationSetting.toDao());
    if (status) {
      return ResultSuccess(data: localizationSetting);
    } else {
      return ResultError(error: Exception("Failed save localization"));
    }
  }
}
