import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/ui/localization_setting_entity.dart';

abstract class SettingRepositoryService {
  Future<Result<LocalizationSettingEntity>> getLocalizationSetting();
  Future<Result<LocalizationSettingEntity>> setLocalizationSetting(LocalizationSettingEntity localizationSetting);
  Future<Result<dynamic>> removeLocalizationSetting();
}