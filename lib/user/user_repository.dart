import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/response/profile.dart';
import 'package:core_network/network_data_source_service.dart';
import 'package:dio/dio.dart';

import 'user_repository_service.dart';

class UserRepository implements UserRepositoryService{

  UserRepository({required this.networkDataSourceService});

  final NetworkDataSourceService networkDataSourceService;

  @override
  Future<BaseResponse<Profile<dynamic>>> getProfile(CancelToken? cancelToken) {
    return networkDataSourceService.getProfile(cancelToken);
  }
}