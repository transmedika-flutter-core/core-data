import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/response/profile.dart';
import 'package:dio/dio.dart';

abstract class UserRepositoryService{
  Future<BaseResponse<Profile<dynamic>>> getProfile(CancelToken? cancelToken);
}